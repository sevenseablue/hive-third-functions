package udf.string;

import org.apache.hadoop.io.Text;
import org.junit.Test;

public class ArrayHistTest {

    @Test
    public void evaluate() {
        int[] a1 = new int[]{1, 15, 101};
        String s1 = "1,15,101";
        String s2 = "1,10,100";
        System.out.println(new ArrayHist().evaluate(new Text(s1), new Text(s2)));
    }
}