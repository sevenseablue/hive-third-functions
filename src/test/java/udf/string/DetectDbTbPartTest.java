package udf.string;

import org.apache.hadoop.io.Text;
import org.junit.Test;

public class DetectDbTbPartTest {

    @Test
    public void evaluate() {
        System.out.println(new DetectDbTbPart().evaluate(new Text("/user/hive/warehouse/wirelessdata.db/ods_client_behavior_session_hour4spark/dt=2017-12-05/hms=05/platform=ios/")));
        System.out.println(new DetectDbTbPart().evaluate(new Text("/user/hive/warehouse/wirelessdata.db/ods_client_behavior_session_hour4spark/dt=2017-12-05/hms=05/platform=ios")));
        System.out.println(new DetectDbTbPart().evaluate(new Text("/user/hive/warehouse/wirelessdata/ods_client_behavior_session_hour4spark/dt=2017-12-05/hms=05/platform=ios")));
        System.out.println(new DetectDbTbPart().evaluate(new Text("/user/hive/warehouse/wirelessdata/ods_client_behavior_session_hour4spark/dt=2017-12-05/hms=05/platform=ios/")));
        System.out.println(new DetectDbTbPart().evaluate(new Text("/user/hive/warehouse/wirelessdata/ods_client_behavior_session_hour4spark/dt2017-12-05/hms05/platformios/")));
        System.out.println(new DetectDbTbPart().evaluate(new Text("/user/hive/warehouse/market.db/yuebaolinshibiao_7yuefen/")));
        System.out.println(new DetectDbTbPart().evaluate(new Text("/user/hive/warehouse/market.db/yuebaolinshibiao_7yuefen/heihei/")));
    }
}