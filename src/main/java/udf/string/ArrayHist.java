package udf.string;

import com.google.common.base.Joiner;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

@Description(name = "arrayHistgram"
        , value = "_FUNC_(string, string) - string split to array by ','. arr1 is histgram on arr2. "
        , extended = "Example:\n > select _FUNC_(string, string) from src;")
public class ArrayHist extends UDF {

    public ArrayHist() {
    }

    public double[] stringToArrayDouble(String str){
        String[] arr1 = str.split(",");
        double[] d1 = new double[arr1.length];
        for(int i=0; i<d1.length; i++){
            d1[i] = Double.parseDouble(arr1[i]);
        }

        return d1;
    }

    public <T> String[] arrayToStringArr(T[] arr){
        String[] res = new String[arr.length];
        for(int i=0; i<arr.length; i++){
            res[i] = String.valueOf(arr[i]);
        }
        return res;
    }


    public Text evaluate(Text arrstr, Text binstr) {
        if (arrstr == null || arrstr == null || arrstr.toString().length()==0 || binstr.toString().length() == 0) {
            return new Text("0");
        }
        double[] d1 = stringToArrayDouble(arrstr.toString());
        double[] d2 = stringToArrayDouble(binstr.toString());
        Integer[] binNum = new Integer[d2.length+1];
        for(int i=0; i<binNum.length; i++){
            binNum[i] = 0;
        }
        for (int i=0; i<d1.length; i++){
            int j;
            for (j=0; j<d2.length; j++){
                if (d1[i] <= d2[j]){
                    binNum[j] += 1;
                    break;
                }
            }
            if (j==d2.length){
                binNum[j] += 1;
            }
        }
        return new Text(Joiner.on(",").join(arrayToStringArr(binNum)));
    }
}

