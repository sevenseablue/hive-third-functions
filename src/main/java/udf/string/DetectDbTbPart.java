package udf.string;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

@Description(name = "detectdb"
        , value = "_FUNC_(string) - ."
        , extended = "Example:\n > select _FUNC_(string) from src;")
public class DetectDbTbPart extends UDF {
    private Text result = new Text();

    public DetectDbTbPart() {
    }

    /**
     * md5 hash.
     *
     * @param text
     * @return
     */
    public Text evaluate(Text text) {
        String db = "";
        String tb = "";
        String partStr = "";

        if (text == null) {
            result.set(db + "\t" + tb + "\t" + partStr + "\t");
            return result;
        }

        String str = text.toString();
        if (str.endsWith("/")) {
            str = str.substring(0, str.length() - 1);
        }
        String[] spli = str.split("/");
        if (str.contains(".db/")) {
            int dbind = str.indexOf(".db/") + 4;
            db = str.substring(0, dbind);
            String tail = str.substring(dbind);
            if(tail.indexOf("/")>0) {
                tb = tail.substring(0, tail.indexOf("/"));
                tail = tail.substring(tail.indexOf("/") + 1);
                partStr = tail + "/";
            }
            else {
                tb = tail;
                partStr = "/";
            }
        } else {
            int flag = 2;
            for (int i = spli.length - 1; i >= 0; i--) {
                if (flag == 2 && spli[i].indexOf("=") >= 0) {
                    partStr = spli[i] + "/" + partStr;
                } else if (flag == 2 && spli[i].indexOf("=") < 0) {
                    flag = 1;
                    tb = spli[i];
                } else if (flag == 1) {
                    db = spli[i] + "/" + db;
                }
            }
        }
        result.set(db + "\t" + tb + "\t" + partStr + "\t");

        return result;
    }
}

