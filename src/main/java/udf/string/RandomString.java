package udf.string;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.hive.ql.exec.vector.VectorizedExpressions;
import org.apache.hadoop.hive.ql.exec.vector.expressions.FuncRand;
import org.apache.hadoop.hive.ql.exec.vector.expressions.FuncRandNoSeed;
import org.apache.hadoop.hive.ql.udf.UDFType;

import java.util.Random;

/**
 * Created by jinxiu.qi on 16-7-29.
 */
@UDFType(deterministic = false)
@VectorizedExpressions({FuncRandNoSeed.class, FuncRand.class})
public class RandomString extends UDF {

    private Random random;

    public String evaluate() {
        return RandomStringUtils.random(32);
    }

    public String evaluate(int length) {
        return RandomStringUtils.random(length);
    }
}
