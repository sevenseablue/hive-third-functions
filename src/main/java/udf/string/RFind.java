package udf.string;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

@Description(name = "rfind"
        , value = "_FUNC_(string) - get the target substring index from right."
        , extended = "Example:\n > select _FUNC_(string) from src;")
public class RFind extends UDF {
    private Text result = new Text();
    private IntWritable intw = new IntWritable();

    public RFind() {
    }

    /**
     * md5 hash.
     *
     * @param text
     * @return
     */
    public IntWritable evaluate(Text sub, Text text) {
        if (text == null || sub == null) {
            return null;
        }

        intw.set(text.toString().lastIndexOf(sub.toString()) + 1);
        return intw;
    }
}

