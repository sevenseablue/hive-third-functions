package udf.array;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentTypeException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ListObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorConverters;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorConverters.Converter;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.Text;

/**
 * @author ruifeng.shan
 * @date 2016-07-26
 * @time 17:31
 */
// TODO string split, join
@Description(name = "arraysub_join"
        , value = "_FUNC_(array<E>, s, e, delimiter, null_replacement) - concatenates s:e of the elements of the given array using the delimiter and an optional null_replacement to replace nulls."
        , extended = "Example:\n > select _FUNC_(array, s, e, delimiter) from src;\n> select _FUNC_(array, s, e, delimiter, null_replacement) from src;")
public class UDFArraySubJoin extends GenericUDF {

    private static final int ARRAY_IDX = 0;
    private static final int START_IDX = 1;
    private static final int END_IDX = 2;
    private static final int DELIMITER_IDX = 3;
    private static final int NULL_REPLACE_IDX = 4;
    private static final int MIN_ARG_COUNT = 4; // min Number of arguments to this UDF
    private static final int MAX_ARG_COUNT = 5; // max Number of arguments to this UDF
    private transient ObjectInspector startOI;
    private transient ObjectInspector endOI;
    private transient ObjectInspector delimiterOI;
    private transient ObjectInspector nullReplaceOI;
    private transient ListObjectInspector arrayOI;
    private transient ObjectInspector arrayElementOI;
    private transient Converter startConvert;
    private transient Converter endConvert;
    private transient Converter delimiterConvert;
    private transient Converter nullReplaceConvert;
    private Text result;

    public UDFArraySubJoin() {

    }

    @Override
    public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
        // Check if two arguments were passed
        if (arguments.length > MAX_ARG_COUNT || arguments.length < MIN_ARG_COUNT) {
            throw new UDFArgumentLengthException(
                    "The function array_join(array, delimiter) or array_join(array, delimiter, null_replacement) takes exactly "
                            + MIN_ARG_COUNT + " or " + MAX_ARG_COUNT + " arguments.");
        }

        // Check if ARRAY_IDX argument is of category LIST
        if (!arguments[ARRAY_IDX].getCategory().equals(ObjectInspector.Category.LIST)) {
            throw new UDFArgumentTypeException(ARRAY_IDX,
                    "\"" + org.apache.hadoop.hive.serde.serdeConstants.LIST_TYPE_NAME + "\" "
                            + "expected at function array_join, but "
                            + "\"" + arguments[ARRAY_IDX].getTypeName() + "\" "
                            + "is found");
        }

        arrayOI = (ListObjectInspector) arguments[ARRAY_IDX];
        arrayElementOI = arrayOI.getListElementObjectInspector();

        startOI = arguments[START_IDX];
        endOI = arguments[END_IDX];
        delimiterOI = arguments[DELIMITER_IDX];
        delimiterConvert = ObjectInspectorConverters.getConverter(delimiterOI, PrimitiveObjectInspectorFactory.writableStringObjectInspector);
        startConvert = ObjectInspectorConverters.getConverter(startOI, PrimitiveObjectInspectorFactory.writableIntObjectInspector);
        endConvert = ObjectInspectorConverters.getConverter(endOI, PrimitiveObjectInspectorFactory.writableIntObjectInspector);
        if (arguments.length == MAX_ARG_COUNT) {
            nullReplaceOI = arguments[NULL_REPLACE_IDX];
            nullReplaceConvert = ObjectInspectorConverters.getConverter(nullReplaceOI, PrimitiveObjectInspectorFactory.writableStringObjectInspector);
        }

        result = new Text();

        return PrimitiveObjectInspectorFactory.writableStringObjectInspector;
    }

    @Override
    public Object evaluate(DeferredObject[] arguments) throws HiveException {
        result.set("");

        Object array = arguments[ARRAY_IDX].get();
        Object delimiter = arguments[DELIMITER_IDX].get();
        Object nullReplace = null;
        if (arguments.length == MAX_ARG_COUNT) {
            nullReplace = arguments[NULL_REPLACE_IDX].get();
        }

        Object startidx = arguments[START_IDX].get();
        Object s = startConvert.convert(startidx);
        Object endidx = arguments[END_IDX].get();
        Object e = endConvert.convert(endidx);

        int arrayLength = arrayOI.getListLength(array);
        int si = Integer.parseInt(s.toString());
        int ei = Integer.parseInt(e.toString());

        // Check if array is null or empty or value is null
        if (array == null || arrayLength <= 0 || arrayLength<=si) {
            return result;
        }
        int endend = Math.min(arrayLength, ei);

        StringBuffer stringBuffer = new StringBuffer();

        Object listElement = arrayOI.getListElement(array, si);
        appendElement(stringBuffer, listElement, nullReplace);
        for (int i = si+1; i < endend; ++i) {
            stringBuffer.append(delimiterConvert.convert(delimiter));
            listElement = arrayOI.getListElement(array, i);
            appendElement(stringBuffer, listElement, nullReplace);
        }
        result.set(stringBuffer.toString());

        return result;
    }

    private void appendElement(StringBuffer stringBuffer, Object listElement, Object nullReplace) {
        if (listElement == null) {
            if (nullReplace != null) {
                stringBuffer.append(nullReplaceConvert.convert(nullReplace));
            } else {
                stringBuffer.append(listElement);
            }
        } else {
            stringBuffer.append(listElement);
        }
    }

    @Override
    public String getDisplayString(String[] strings) {
        assert (strings.length <= MAX_ARG_COUNT || strings.length >= MIN_ARG_COUNT);
        if (strings.length == 2) {
            return "array_join(" + strings[ARRAY_IDX] + ", "
                    + strings[DELIMITER_IDX] + ")";
        } else {
            return "array_join(" + strings[ARRAY_IDX] + ", "
                    + strings[DELIMITER_IDX] + ", "
                    + strings[NULL_REPLACE_IDX] + ")";
        }

    }
}