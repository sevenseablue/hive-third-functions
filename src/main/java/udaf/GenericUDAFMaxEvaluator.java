package udaf;

import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDAFEvaluator;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorUtils;

public class GenericUDAFMaxEvaluator extends GenericUDAFEvaluator {
    private transient ObjectInspector inputOI;
    private transient ObjectInspector outputOI;

    @Override
    public ObjectInspector init(Mode m, ObjectInspector[] parameters)
            throws HiveException {
        assert (parameters.length == 1);
        super.init(m, parameters);
        inputOI = parameters[0];
        // Copy to Java object because that saves object creation time.
        // Note that on average the number of copies is log(N) so that's not
        // very important.
        outputOI = ObjectInspectorUtils.getStandardObjectInspector(inputOI,
                ObjectInspectorUtils.ObjectInspectorCopyOption.JAVA);
        return outputOI;
    }

    /**
     * class for storing the current max value
     */
    static class MaxAgg extends AbstractAggregationBuffer {
        Object o;
    }

    @Override
    public AggregationBuffer getNewAggregationBuffer() throws HiveException {
        MaxAgg result = new MaxAgg();
        return result;
    }

    @Override
    public void reset(AggregationBuffer agg) throws HiveException {
        MaxAgg myagg = (MaxAgg) agg;
        myagg.o = null;
    }

    boolean warned = false;

    @Override
    public void iterate(AggregationBuffer agg, Object[] parameters)
            throws HiveException {
        assert (parameters.length == 1);
        merge(agg, parameters[0]);
    }

    @Override
    public Object terminatePartial(AggregationBuffer agg) throws HiveException {
        return terminate(agg);
    }

    @Override
    public void merge(AggregationBuffer agg, Object partial)
            throws HiveException {
        if (partial != null) {
            MaxAgg myagg = (MaxAgg) agg;
            int r = ObjectInspectorUtils.compare(myagg.o, outputOI, partial, inputOI);
            if (myagg.o == null || r < 0) {
                myagg.o = ObjectInspectorUtils.copyToStandardObject(partial, inputOI, ObjectInspectorUtils.ObjectInspectorCopyOption.JAVA);
            }
        }
    }

    @Override
    public Object terminate(AggregationBuffer agg) throws HiveException {
        MaxAgg myagg = (MaxAgg) agg;
        return myagg.o;
    }
}